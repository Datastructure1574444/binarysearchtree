/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.tree;

/**
 *
 * @author HP
 */
public class InOrderInTraversal {
    public static void main(String[] args) {
        BinaryTree tree = new BinaryTree();

        tree.insert(50);
        tree.insert(30);
        tree.insert(70);
        tree.insert(20);
        tree.insert(40);
        tree.insert(60);
        tree.insert(80);
        tree.inOrderTraversal(tree.root); 

        //delete node
        System.out.println();
        int valueToDelete = 40;
        tree.delete(valueToDelete);
        tree.inOrderTraversal(tree.root);

    }

}
